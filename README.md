# Role Name

Ansible role to install Jenkins JNLP Agents on Linux. Multiple agent instances per host possible.
Right now only Linux nodes supported but Windows nodes should be possible as well.

![jennkins_screenshot](./.img/jenkins001.png)

## Dependencies

No other roles required.

## Setup

Use __galaxy install__ to download and install the role to your Ansible workspace.

File _ansible.cfg_ :

```ini
[defaults]
host_key_checking = True
http_user_agent = ansible-agent
inventory = ./hosts.yml
log_path = ./.log/ansible.log
roles_path = ./roles
```

File _requirements.yml_ :

```yaml
---
- src: https://gitlab.com/fwalk/ansible-jenkins-jnlp-agent.git
  scm: git
  version: master
  name: ansible-jenkins-jnlp-agent
```

Run _galaxy install_ :

```sh
ansible-galaxy install -r requirements.yml --roles-path roles
```

## Role Variables

| Name                     | Default                           | Description                          |
|--------------------------|-----------------------------------|--------------------------------------|
| jenkins_agent_add_user   | false                             | Create users with _useradd_ .        |
| jenkins_agent_deploy_agent | false                           | Download _agent.jar_ .               |
| jenkins_agent_discover_conf | false                          | Query Jenkins API for Agent config.  |
| jenkins_agent_discover_host | _none_                         | Jenkins hostname for config discovery. |
| jenkins_agent_discover_password | _none_                     | User password for config discovery. |
| jenkins_agent_discover_user | _none_                         | User name for config discovery.     |
| jenkins_agent_jar        | /usr/share/jenkins/slave.jar      | Full name of agent.jar on the agent. |
| jenkins_agent_java_bin   | /usr/bin/java                     | Full name of the _java_ executable.  |
| jenkins_agent_java_opts  | _none_                            | Java opts for the agent process.     |
| jenkins_agent_jnlp_opts  | -Dorg.jenkinsci.remoting.engine.JnlpProtocol3.disabled=true | JNLP Java opts for the agent process. |
| jenkins_agent_work_dir   | .jenkins-agent                    | Working directory name, if not specified otherwise |
| jenkins_url              | http://localhost:8080             | Jenkins server URL.                  |
| ---                      | ---                               | ---                                  |
| jenkins_agent_instances  | -                                 | Agent definitions:                   |
| - name                   | _none_                            | Optional, if discover mode is activ. |
| - secret                 | _none_                            | Optional, if discover mode is activ. |
| - user                   | _none_                            | Mandatory, user for the agent service. |
| - work_dir               | _inventory mode_ : &lt;home&gt;/&lt;jenkins_agent_work_dir&gt; | Optional, if discover mode is activ. |

## Example

File _install_ansible-jenkins-jnlp-agent.yml_ :

```yaml
---
- name: Jenkins JNLP Agent
  hosts: jenkins_agent
  roles:
    - role: ansible-jenkins-jnlp-agent
```

File _hosts.yaml_ :

```yaml
all:
  hosts:
    jenkins-agent-1:
      jenkins_agent_add_user: true
      jenkins_agent_instances:
        - user: user111
          num_executors: 4
        - name: JenkinsSlaveEins.user222
          user: user222
          work_dir: /data/jenkins/user222
    jenkins-agent-2:
      jenkins_agent_add_user: true
      jenkins_agent_instances:
        - name: NODE
          user: user333
          secret: 012345678901234567890123456789
          work_dir: /var/lib/jenkins-agent/_jenkins_NODE
jenkins_agent:
  hosts:
    jenkins-agent-[1:2]
  vars:
    jenkins_url: http://10.0.0.10:8080
    jenkins_agent_deploy_agent: true
    jenkins_agent_add_user: true
    #
    jenkins_agent_discover_conf: true
    jenkins_agent_discover_host: jenkins-1
    jenkins_agent_discover_user: admin
    jenkins_agent_discover_password: admin  
```

```sh
ansible-playbook install_ansible-jenkins-jnlp-agent.yml
```

## Molecule

[Molecule](https://molecule.readthedocs.io/en/stable/) framework provides capabilities to test the Ansible Roles with infrastructure created dynamically by using Vagrant, Virtualbox, Docker, etc.

### Setup

It's recommended to create a Python virtual-environment (venv) first:

```sh
mkdir -p ~/venv/molecule
virtualenv ~/venv/molecule
```

```sh
pip install ansible
pip install molecule
pip install docker-py
pip install python-vagrant
```

### Example

```shell
molecule converge
molecule test --destroy never
```

Please take attention to append __--destroy never__ because otherwise _molecule create_ must executed again in case of failed tests.

## Author Information

fwalk___gitlab
