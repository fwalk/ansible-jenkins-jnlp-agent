import os
import pytest
# import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('jenkins_agent')


@pytest.mark.parametrize('file, content', [
  ("/etc/systemd/system/jenkins-agent@.service", "AGENT_NAME")
])
def test_files(host, file, content):
    file = host.file(file)

    assert file.exists
    assert file.contains(content)
